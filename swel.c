#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

extern int luaopen_lpeg(lua_State*);

extern const char _binary_swel_luac_start[];
extern const char _binary_swel_luac_end[];

void die(const char *fmt, ...);
int lua_frombin(lua_State*);
void loadembed(lua_State *L, const char *name, const char *start, const char *end);

struct luaL_Reg rfuncs[] = {
	{"frombin",	lua_frombin},
	{NULL,	NULL}
};

int
main(int argc, char* argv[])
{
	lua_State *lua;
	lua = luaL_newstate();
	luaL_openlibs(lua);
	
	lua_getglobal(lua, "package");
	lua_getfield(lua, -1, "preload");
	lua_pushcfunction(lua, luaopen_lpeg);
	lua_setfield(lua, -2, "lpeg");
	lua_pop(lua, 2);
	
	luaL_newlib(lua, rfuncs);
	lua_setglobal(lua, "swel");
	
	loadembed(lua, "swel.lua", _binary_swel_luac_start, _binary_swel_luac_end);
	lua_call(lua, 0, 0);
	lua_getglobal(lua, "repl");
	lua_call(lua, 0, 0);
	lua_close(lua);
	return 0;
}

void
loadembed(lua_State *L, const char *name, const char *start, const char *end)
{
	switch (luaL_loadbuffer(L, start, end - start, name)) {
	case LUA_ERRSYNTAX:
		die("Lua returned a syntax error while loading compiled boot code from %s. This should be impossible.", name);
		break;
	case LUA_ERRMEM:
		die("out of memory");
		break;
	case LUA_ERRGCMM:
		die("error while running a __gc metamethod");
		break;
	}
}

int
lua_frombin(lua_State *L)
{
	lua_Integer v = 0;
	const char *s, *arg = luaL_checkstring(L, 1);
	if (!arg[0] || arg[0] != '0' || !arg[1] || arg[1] != 'b' || !arg[2])
		return luaL_error(L, "invalid binary constant \"%s\"", arg);
	for (s = arg+2; *s; s++) {
		v <<= 1;
		if (*s == '1')
			v |= 1;
		else if (*arg != '0')
			return luaL_error(L, "invalid binary constant \"%s\"", arg);
	}
	lua_pushinteger(L, v);
	return 1;
}

void
die(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);

	if (fmt[0] && fmt[strlen(fmt)-1] == ':') {
		fputc(' ', stderr);
		perror(NULL);
	}

	exit(1);
}
