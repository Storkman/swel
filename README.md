Swel — a calculator
===============

The interpreter does not currently support any kind of line editing. This can be handled externally by a wrapper like [rlwrap](https://github.com/hanslub42/rlwrap).

All external dependencies (currently Lua and LPeg) are included in this repository, and are statically compiled, together with the interpreter script, into a single executable.

Syntax
------

	> 1 + cos^2 60deg
	   = 1.25

The five basic arithmetic operators (`+`, `-`, `*`, `/`, `^`) work as usual.

Operators written without spaces around them (`1+2*2`) are evaluated before operators with spaces (`1 + 2 * 2`).

	> 10 * 1 + 4
	   = 14
	> 10 * 1+4
	   = 50

Operators with a space on one side only behave the same as operators with spaces on both sides.

Values can be multiplied simply by writing them together.

	> 2 2 2
	   = 8
	> cos 2pi
	   = 1.0

This "space multiplication" is applied after "un-spaced" arithmetic operators, but before "spaced" operators.

Parenthesis are closed automatically at the end of a line.

	> 1 / ( sqrt 2pi
	   = 0.39894228040143

The pipe symbol `|` takes the entire expression on its left side as a single value.

	> 2.1 + 2.2 + 3.1 + 2.8 + 2.9
	   = 13.1
	> 2.1 + 2.2 + 3.1 + 2.8 + 2.9 | /5
	   = 2.62

The pipe symbol at the beginning of a line represents the result of the last expression.

	> 365 * 24 * 60 * 60
	   = 157766400.0
	> | / 1M
	   = 157.7664

Functions
--------
Multiplying on the right is used for function calls. Multiplying on the left works as usual.

	> cos 2pi
	   = 1.0
	> 2 * cos 2pi
	   = 2.0

Functions can also be composed with arithmetic operators.

	> (cos+1) 2pi
	   = 2.0
	> (sin^2 + cos^2) 0.12345
	   = 1.0

Built-in Functions and Constants
--------------
`deg` is defined as π/180, so `x deg` converts `x` degrees to radians, `x /deg` converts `x` radians to degrees.
SI suffixes are defined as constants from `n` to `P`. Their binary equivalents (powers of 1024) are defined from `kb` to `Pb`.

The following symbols from the Lua math library are exposed: `abs`, `acos`, `asin`, `atan`, `ceil`, `cos`, `exp`, `floor`, `log`, `pi`, `sin`, `sqrt`, `tan`.

`ln` is an alias for `log`. `log2` and `log10` are the base-2 and base-10 logarithm, repsectively. `sq` is an alias for `sqrt`. `e` is equal to `exp 1`

Dependencies
-----------

 - Lua – https://www.lua.org/
 - LPeg – http://www.inf.puc-rio.br/~roberto/lpeg/lpeg.html
