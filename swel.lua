local lp = require "lpeg"

local C, Cc, Cp, Ct, P, R, S, V = lp.C, lp.Cc, lp.Cp, lp.Ct, lp.P, lp.R, lp.S, lp.V

local Name = R("az", "AZ", "__") * R("az", "AZ", "__", "09")^0

local fnum =
	R"09"^0 * '.' * R"09"^1 * ('e' * P'-'^-1 * R"09"^1)^-1 +
	R"09"^1 * ('e' * P'-'^-1 * R"09"^1)^-1
local bnum = P"0b" * (S"01")^1
local hnum = P"0x" * R("09", "af", "AF")^1
local Number = bnum /swel.frombin + hnum /tonumber + fnum /tonumber

local space = S" \t\r"
local commentStart = P'#'
local comment = P'#' * P(1)^0

local ExprMeta = {__type = "swel/expr"}
ExprMeta.__index = ExprMeta
local function Expr(arg)
	local valid = {value = true, name = true, binop = true, unop = true, continued = true, vec = true}
	if not valid[arg.t] then
		error("invalid expression type: "..arg.t)
	end
	setmetatable(arg, ExprMeta)
	return arg
end

function ExprMeta:is(t) return self.t == t end

local function isexpr(e)
	local m = getmetatable(e)
	return m and m.__type == "swel/expr"
end
	
local function checkexpr(e, argn)
	if isexpr(e) then return end
	if argn then
		error("argument #"..argn.." must be a parsed expression", 2)
	end
	error("argument must be a parsed expression", 2)
end

local function name(n, pos) return Expr{n, t="name", pos=pos} end
local function tag(n, pos) return Expr{t=n, pos=pos} end

-- A snip takes the entire input on its left and turns it into an operand. It can only occur on the left side of a binary operator or at the end of input.
local Snip = space^0 * ("|" * Cc"continued" * Cp()) / tag

-- Parenthesis is automatically closed on end of input, snip, or start of comment.
local Paren = P'(' *space^0* V"Expr" *space^0* ( P')' + -P(1) + #Snip + #commentStart )

swel.missing = {}

local function op(name)
	return Cc(name)
end

local function binop(op, b, pos)
	if not op then return a end
	return {op, b, pos=pos}
end

local function binopseq(pat)
	return lp.Cf(pat, function(seq, op) return Expr{seq, op[2], op[1], t="binop", pos=op.pos} end)
end

local function mkbinop(Next, tok, optype)
	return tok*op(optype)*(Next-Snip) + tok*op(optype) * -P(1) * Cc(missing) * Cp() 
end

local function mkbinops(Next, tokA, opA, tokB, opB)
	return binopseq((Next+Snip) *
		( (mkbinop(Next, tokA, opA) + mkbinop(Next, tokB, opB)) / binop )^0)
end

local function unop(op, arg)
	if not arg then return op end
	return Expr{arg, op, t="unop"}
end

local function arithmetic(mod, Next)
	local plus, minus, mul, div, pow = mod'+', mod'-', mod'*', mod'/', mod'^'
	local Pow = binopseq((V(Next)+Snip) * (mkbinop(V(Next), pow, "^") / binop)^0)
	local Prod = mkbinops(Pow, mul, "*", div, "/")
	local Sum = mkbinops(Prod, plus, "+", minus, "-")
	return Sum
end

local function vector(xs)
	if #xs == 1 then
		return xs[1]
	end
	xs.t = "vec"
	return Expr(xs)
end

local function spaced(p)
	return space^1*P(p)*space^0 + space^0*P(p)*space^1
end

swel.grammar = P{"Line",
	Line = space^0 * (comment*Cc(nil) + V"Expr" * (V"Snipped" + comment)^-1);
	Snipped = #Snip * Cp();
	
	Expr = V"Vec";
	-- cos, 5, ( expression )
	Factor = ( C(Name)*Cp() )/name + Number + Paren;
	-- 2x
	MultiFact = binopseq(V"Factor" * ((op"*" * V"Factor")/binop)^-1);
	-- -2x
	NegMultiFact = ( (P'-' * Cc'-')^-1 * V"MultiFact") / unop;
	-- 2*x+1
	Strong = arithmetic(P, "NegMultiFact");
	-- 2 sin x
	SpaceProd = binopseq(V"Strong" * (mkbinop(V"Strong", space, "*") / binop)^0);
	-- 1.5 * 3600
	Weak = arithmetic(spaced, "SpaceProd");
	-- (1, 2, 3)
	Vec = Ct(V"Weak" * (space^0* P"," *space^0* V"Weak")^0) / vector;
}

local function _subst(expr, binds, unbound)
	if not isexpr(expr) then return expr end
	
	if expr:is"name" then
		local v = binds[expr[1]]
		if not v then
			unbound[expr[1]] = expr.pos
			return expr
		end
		return v
	elseif expr:is"binop" then
		local a, b = _subst(expr[1], binds, unbound), _subst(expr[2], binds, unbound)
		return Expr{a, b, expr[3], t="binop"}
	elseif expr:is"unop" then
		return Expr{_subst(expr[1], binds, unbound), expr[2], t="unop"}
	elseif expr:is"value" then
		return expr
	elseif expr:is"vec" then
		local vec = {t="vec"}
		for i, v in ipairs(expr) do
			vec[i] = _subst(v, binds, unbound)
		end
		return Expr(vec)
	end
	
	error("can't substitute an expression of type \""..expr.t.."\"")
end

local FuncMeta = {__type = "swel/func"}
local function Func(x)
	local f = {x}
	setmetatable(f, FuncMeta)
	return f
end
swel.Func = Func

local function isfunc(f)
	local m = getmetatable(f)
	return m and m.__type == "swel/func"
end

function FuncMeta.__call(f, ...)
	return f[1](...)
end

function FuncMeta.__add(a, b)
	local fa, fb = isfunc(a), isfunc(b)
	if fa and fb then
		return Func(function(x) return a[1](x) + b[1](x) end)
	elseif fa then
		return Func(function(x) return a[1](x) + b end)
	else
		return Func(function(x) return a + b[1](x) end)
	end
end

function FuncMeta.__sub(a, b)
	local fa, fb = isfunc(a), isfunc(b)
	if fa and fb then
		return Func(function(x) return a[1](x) - b[1](x) end)
	elseif fa then
		return Func(function(x) return a[1](x) - b end)
	else
		return Func(function(x) return a - b[1](x) end)
	end
end

function FuncMeta.__mul(a, b)
	local fa, fb = isfunc(a), isfunc(b)
	if fa and fb then
		local a, b = a[1], b[1]
		return Func(function(x) return a(b(x)) end)
	elseif fa then
		return a[1](b)
	else
		local b = b[1]
		return Func(function(x) return a * b(x) end)
	end
end

function FuncMeta.__div(a, b)
	local fa, fb = isfunc(a), isfunc(b)
	if fa and fb then
		local a, b = a[1], b[1]
		return Func(function(x) return a[1](x) / b[1](x) end)
	elseif fa then
		return Func(function(x) return a[1](x) / b end)
	else
		return Func(function(x) return a / b[1](x) end)
	end
end

function FuncMeta.__pow(a, b)
	local fa, fb = isfunc(a), isfunc(b)
	if fa and fb then
		error "invalid operation"
	elseif fa then
		local f = a[1]
		return Func(function(x) return f(x) ^ b end)
	else
		local f = b[1]
		return Func(function(x) return a ^ f(x) end)
	end
end

function FuncMeta.__unm(a)
	local f = a[1]
	return Func(function(x) return -f(x) end)
end

local function _exec(ex)
	if type(ex) == "function" then
		return Func(ex)
	end
	
	if not isexpr(ex) then return ex end
	
	if ex:is"binop" then
		local a, b = _exec(ex[1]), _exec(ex[2])
		if ex[3] == "+" then return a + b
		elseif ex[3] == "-" then return a - b
		elseif ex[3] == "*" then return a * b
		elseif ex[3] == "/" then return a / b
		elseif ex[3] == "^" then return a ^ b
		else error("unknown binop "..ex[3], 2) end
	end
	
	if ex:is"unop" then
		local a = _exec(ex[1])
		if ex[2] == "-" then return -a
		else error("unknown unop "..ex[2], 2) end
	end
	
	if ex:is"value" then
		return ex[1]
	end
	
	if ex:is"vec" then
		local vec = {}
		for i, v in ipairs(ex) do
			vec[i] = _exec(v)
		end
		return vec
	end

	if ex:is"name" then
		error("unbound name: "..ex[1])
	end
	
	error("invalid expression type: "..ex.t)
end

local luamathf = {
	"abs",
	"acos",
	"asin",
	"atan",
	"ceil",
	"cos",
	"exp",
	"floor",
	"log",
	"sin",
	"sqrt",
	"tan",
}

swel.names = {
	ln = Func(math.log),
	log2 = Func(function(x) return math.log(x, 2) end),
	log10 = Func(function(x) return math.log(x, 10) end),
	sq = Func(math.sqrt),
	pi = math.pi,
	deg = math.pi / 180;
	e = math.exp(1),
	
	n = 1e-9, u = 1e-6, m = 1e-3,
	k = 1e3, M = 1e6, G = 1e9, T = 1e12, P = 1e15,
	
	kb = 1024, Mb = 1024^2, Gb = 1024^3, Tb = 1024^4, Pb = 1024^5,
}

for _, n in ipairs(luamathf) do
	swel.names[n] = Func(math[n])
end

swel.error = {
	unbound = "unbound names",
}

function swel.show(v, out)
	out = out or io.stdout
	local t = type(v)
	if type(v) == "function" then
		out:write "<function>"
		return
	end
	
	if type(v) ~= "table" then
		out:write(v or "nil")
		return
	end
	
	out:write "{"
	local first = true
	local shown = {}
	for k, e in ipairs(v) do
		if not first then out:write ', ' end
		first = false
		swel.show(e, out)
		shown[k] = true
	end
	
	for k, e in pairs(v) do
		if not shown[k] then
			if not first then out:write ', ' end
			first = false
			swel.show(k, out)
			out:write " = "
			swel.show(e, out)
		end
	end
	out:write "}"
end

local function continue(last, new)
	if not isexpr(new) or new:is"value" or new:is"name" then
		return new
	end
	
	if new:is"continued" then
		return last or Expr{"ANS", t="name"}
	end
	
	if new:is"binop" then
		new[1] = continue(last, new[1])
		new[2] = continue(last, new[2])
		return new
	end
	
	if new:is"unop" then
		new[1] = continue(last, new[1])
		return new
	end
	
	if new:is"vec" then
		for i = 1,#new do
			new[i] = continue(last, new[i])
		end
		return new
	end
	
	error("invalid expression type: "..new.t)
end

function swel.subst(expr, binds)
	local ub = {}
	local e = _subst(expr, binds, ub)
	
	local unbound = false
	for n, p in pairs(ub) do
		unbound = ub
		break
	end
	
	return e, unbound or nil
end
ExprMeta.subst = swel.subst

function swel.exec(expr, binds)
	checkexpr(expr, 1)
	local ex, unb = expr:subst(binds)
	if unb then
		return nil, swel.error.unbound, unb
	end
	return _exec(ex)
end
ExprMeta.exec = swel.exec

function swel.eval(str, binds)
	return swel.parse(str):exec(binds)
end

local exprMeta = {subst = swel.subst, exec = swel.exec}
exprMeta.__index = exprMeta

function swel.parse(s)
	local e, last = {}, nil
	local where = 1
	while true do
		local snipped = false
		e, snipped = swel.grammar:match(s, where)
		if not e then break end
		last = continue(last, e)
		if not snipped then break end
		where = snipped
	end
	if not isexpr(last) then
		last = Expr{last, t = "value"}
	end
	return last, e
end

local function packEval(sw, str)
	return swel.eval(str)
end
setmetatable(swel, {__call = packEval})

package.loaded.swel = swel
return swel
