#! /usr/bin/env lua

swel = require "swel"

local names = {ANS = 0}
setmetatable(names, {__index = swel.names})

local function showtree(line)
	local v = swel.parse(line)
	swel.show(v)
	io.write '\n'
end

local function run(line)
	local r, err, info = swel.eval(line, names)
	if not err then
		if not r then
			return
		end
		names.ANS = r
		io.write "   = "
		print(r)
		return
	end
	
	if err == swel.error.unbound then
		local names = {}
		for n, _ in pairs(info) do names[#names+1] = n end
		
		if #names > 1 then
			io.write "unbound names: "
		else
			io.write "unbound name: "
		end
		print(table.concat(names, ", "))
		return
	end
	
	print(err)
	swel.show(info)
	io.write "\n"
end

local function dumpstack(err)
	return debug.traceback(err, 2)
end

function repl()
	while true do
		io.write "> "
		local line = io.read"l"
		if not line then return end
		if line:sub(1,1) == "?" then
			showtree(line:sub(2))
		else
			local ok, err = xpcall(run, dumpstack, line)
			if not ok then io.write(err); io.write "\n" end
		end
	end
end

return repl
